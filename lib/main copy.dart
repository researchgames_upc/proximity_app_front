import 'package:flutter/material.dart';
import 'package:proximity/src/routes/routes.dart';
//import 'package:proximity/src/temas/Themes.dart';
//import 'package:flutter/services.dart';

void main() {
  /*  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
 */
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Proximity',
      //theme: proximity_theme,
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: getApplicationRoutes(),
    );
  }
}
