import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proximity/src/routes/routes.dart';
import 'package:proximity/src/services/user_service.dart';
import 'package:proximity/src/utils/preferencias_usuario.dart';

//import 'package:proximity/src/temas/Themes.dart';
//import 'package:flutter/services.dart';
void setupLocator() {
  GetIt.I.registerLazySingleton(() => UserService());
}

void main() async {
  /*  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
 */
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  setupLocator();
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Proximity',
      //theme: proximity_theme,
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: getApplicationRoutes(),
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
