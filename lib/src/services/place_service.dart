import 'dart:io';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/utils/preferencias_usuario.dart';
import 'package:http/http.dart' as http;

class PlaceService with ChangeNotifier {
  final _prefs = new PreferenciasUsuario();
// get places
  Future<List<Place>> getPlaces() {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    return http.get(Uri.parse(url + '/lugar'), headers: headers).then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        final places = [];
        for (var item in jsonData) {
          places.add(Place.fromJson(item));
        }

        return places;
      }
      return [];
    }).catchError((_) => null);
  }

  Future<Place> patchPlaces(String upID, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    return http
        .patch(Uri.parse(url + '/lugar/' + upID),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      if (data.statusCode == 200) {
        Place dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<Place> updatePlaces(String upID, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    return http
        .put(Uri.parse(url + '/lugar/' + upID),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      if (data.statusCode == 200) {
        Place dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<Place> createPlaces(item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    print(jsonv);
    return http
        .post(Uri.parse(url + '/lugar'),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 200) {
        Place dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<Place> deletePlaces(String upID) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    return http
        .delete(Uri.parse(url + '/lugar/' + upID), headers: headers)
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 200) {
        Place dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }
}
