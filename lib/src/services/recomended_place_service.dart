import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:flutter/cupertino.dart';
import 'package:proximity/src/models/marker_response.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/models/recomended_place_model.dart';

import 'package:proximity/src/utils/preferencias_usuario.dart';
import 'package:http/http.dart' as http;

class RecomendedPlaceService with ChangeNotifier {
  final _prefs = new PreferenciasUsuario();
// get places
  Future<List<RecomendedPlace>> getRecomendedPlaces() {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    return http
        .get(Uri.parse(url + '/lugarRecomendado'), headers: headers)
        .then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        final recomendedPlaces = <RecomendedPlace>[];
        for (var item in jsonData) {
          recomendedPlaces.add(RecomendedPlace.fromJson(item));
        }
        return recomendedPlaces;
      }

      return [];
    });
  }

  Future<RecomendedPlace> patchLugarRecomendado(String upID, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    return http
        .patch(Uri.parse(url + '/lugarRecomendado/' + upID),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      if (data.statusCode == 200) {
        RecomendedPlace dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<RecomendedPlace> updateLugarRecomendado(String upID, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    return http
        .put(Uri.parse(url + '/lugarRecomendado/' + upID),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      if (data.statusCode == 200) {
        RecomendedPlace dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<RecomendedPlace> nuevoLugarRecomendado(item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    print(jsonv);
    return http
        .post(Uri.parse(url + '/lugarRecomendado'),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 200) {
        RecomendedPlace dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<RecomendedPlace> deleteLugarRecomendado(String upID) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    return http
        .delete(Uri.parse(url + '/lugarRecomendado/' + upID), headers: headers)
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 200) {
        RecomendedPlace dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  //este es el servicio para obtener los LR cercanos
  Future<List<Marcador>> getMarkers(String range, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    return http
        .post(Uri.parse(url + '/lugarRecomendado/near/' + range),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 201) {
        //print(data.request.toString());
        Iterable l = json.decode(data.body);
        List<Marcador> responsejson =
            List<Marcador>.from(l.map((model) => Marcador.fromJson(model)));

        return responsejson;
      }
      List<Marcador> noData = [];
      return noData;
    }).catchError((_) => null);
  }

  Future<Marcador> getMarkerInfo(item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    return http
        .post(Uri.parse(url + '/lugarRecomendado/info'),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      if (data.statusCode == 201) {
        Marcador dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }

  Future<Place> getPositionPlace(formData) async {
    Dio dio = new Dio();
    /*    final url = _prefs.endPoint;
    final token = _prefs.token; */
    final headers = {
      HttpHeaders.contentTypeHeader: 'multipart/form-data',
      HttpHeaders.acceptHeader: '*/*',
    };
    Response response = await dio.post("http://10.0.2.2:9090/ubicacion",
        data: formData, options: Options(headers: headers));

    if (response.statusCode == 200) {
      print("lat: " +
          response.data["latitud"] +
          " lng: " +
          response.data["longitud"]);
      Place lugarObtenido = new Place();
      print("lugar obtenido: " + response.data["nombre"].toString());
      lugarObtenido.latitud = double.parse(response.data["latitud"]);
      lugarObtenido.longitud = double.parse(response.data["longitud"]);
      lugarObtenido.nombreLugar = response.data["nombre"].toString();
      lugarObtenido.estadoLugar = true;
      lugarObtenido.idLugar = null;

      /*este es el rango que asignas para obtener un una lista en json 
          con todos los lugares que esten dentro de este. el servicio devuelve una lista de json
          */
      print("el lugar es : " + lugarObtenido.toString());
      return lugarObtenido;
    }
    return null;
  }
}
