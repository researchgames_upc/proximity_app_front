import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:proximity/src/models/api_response.dart';

import 'package:proximity/src/models/login_response.dart';
import 'package:proximity/src/models/user_model.dart';
import 'package:proximity/src/utils/preferencias_usuario.dart';

class UserService with ChangeNotifier {
  static const headers = {'Content-Type': 'application/json'};

  final _prefs = new PreferenciasUsuario();

  Future<LoginResponse> login(String uEmail, String uPwd) {
    final endPoint = _prefs.endPoint;
    final authData = {
      'emailUsuario': uEmail,
      'passwordUsuario': uPwd,
    };
    return http
        .post(Uri.parse(endPoint + '/auth/login'),
            headers: headers, body: json.encode(authData))
        .then((data) {
      print("el estado de la solcitud es : " + data.statusCode.toString());
      print("si devuelvo info es : " + data.body.toString());
      if (data.statusCode == 201) {
        final jsonData = json.decode(data.body);
        //final sendData = jsonData["data"];
        //final jsonDataR = json.decode(data.body);
        /* print(
            'el id de usuario es :' + jsonData.data.user.idUsuario.toString()); */
        _prefs.token = jsonData['data']['accessToken'];
        _prefs.iduser = jsonData['data']['user']['idUsuario'];
        _prefs.nombreUser = jsonData['data']['user']['nombreUsuario'];

        return LoginResponse.fromJson(jsonData);
        //data: LoginResponse.fromJson(jsonData["data"]));
      }
      /* print("el endpoint es : " + data.request.toString());
      print("los datos que recibo son : " + authData.toString());
      print("el estado de la solcitud es : " + data.statusCode.toString());
      print("si devuelvo info es : " + data.body.toString()); */
      return LoginResponse(
          message: 'El usuario y/o Contraseña es incorrecto', data: null);
    }).catchError((_) => LoginResponse(
            message: 'ha ocurrido un error no esperado', data: null));
  }

  Future<List<User>> getAdmins() {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    return http
        .get(Uri.parse(url + '/usuario/admin'), headers: headers)
        .then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        final admins = <User>[];
        for (var item in jsonData) {
          admins.add(User.fromJson(item));
        }
        return admins;
      }

      return [];
    });
  }

  Future<User> getUserProfile() {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    return http
        .get(Uri.parse(url + '/auth/profile'), headers: headers)
        .then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        final userp = User.fromJson(jsonData);

        /* for (var item in jsonData) {
          userp.add(User.fromJson(item));
        } */
        return userp;
      }

      return null;
    });
  }

  Future<APIResponse<bool>> nuevoUsuario(UserModel item) {
    final url = _prefs.endPoint;
    return http
        .post(Uri.parse(url + '/usuario'),
            headers: headers, body: json.encode(item))
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 201) {
        return APIResponse<bool>(data: true);
      }
      return APIResponse<bool>(error: true, errorMessage: 'An error occured');
    }).catchError((_) =>
            APIResponse<bool>(error: true, errorMessage: 'An error occured'));
  }

  Future<UserModel> updateUser(String upID, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    print(jsonv);
    return http
        .put(Uri.parse(url + '/usuario/' + upID),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 200) {
        UserModel dato = json.decode(data.body);
        return dato;
      }
      return APIResponse<bool>(error: true, errorMessage: 'An error occured');
    }).catchError((_) => null);
  }

  Future<UserModel> patchUser(String upID, item) {
    final url = _prefs.endPoint;
    final token = _prefs.token;
    final headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };
    var jsonv = item.toJson();
    print(jsonv);
    return http
        .patch(Uri.parse(url + '/usuario/' + upID),
            headers: headers, body: json.encode(jsonv))
        .then((data) {
      print(data.body.toString());
      if (data.statusCode == 200) {
        UserModel dato = json.decode(data.body);
        return dato;
      }

      return null;
    }).catchError((_) => null);
  }
}
