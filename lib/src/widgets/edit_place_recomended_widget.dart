import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/recomended_place_model.dart';
import 'package:proximity/src/services/recomended_place_service.dart';

class RecomendedPlaceEditWidget extends StatefulWidget {
  final RecomendedPlace lugarRecomendado;
  const RecomendedPlaceEditWidget({Key key, this.lugarRecomendado})
      : super(key: key);

  @override
  _RecomendedPlaceEditWidgetState createState() =>
      _RecomendedPlaceEditWidgetState();
}

class _RecomendedPlaceEditWidgetState extends State<RecomendedPlaceEditWidget> {
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  final lugarRecomendadoService = new RecomendedPlaceService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Editar Lugar Recomendado"),
          backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        ),
        body: Container(
            padding: EdgeInsets.all(10.0), child: _formulario(context)));
  }

  Widget _formulario(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              _editNombre(),
              SizedBox(
                height: 5,
              ),
              _editLatitud(),
              SizedBox(
                height: 5,
              ),
              _editLongitud(),
              SizedBox(
                height: 5,
              ),
              _editActividad(),
              SizedBox(
                height: 5,
              ),
              SizedBox(
                height: 30,
              ),
              _editDescripcion(),
              SizedBox(
                height: 5,
              ),
              _editImagen(),
              SizedBox(
                height: 5,
              ),
              _crearBoton(context),
            ],
          )),
    );
  }

  Widget _editNombre() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Nombre:   "),
      initialValue: widget.lugarRecomendado.nombreLugar,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarRecomendado.nombreLugar = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Nombre es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editDescripcion() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Descripcion:   "),
      initialValue: widget.lugarRecomendado.descripcion,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarRecomendado.descripcion = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Descripción es requerida';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editImagen() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Imagen:   "),
      initialValue: widget.lugarRecomendado.imagen,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarRecomendado.imagen = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Imagen es requerida';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editLatitud() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(prefixText: "Latitud:   "),
      initialValue: widget.lugarRecomendado.latitud.toString(),
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarRecomendado.latitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'Latitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editLongitud() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(prefixText: "Longitud:   "),
      initialValue: widget.lugarRecomendado.longitud.toString(),
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarRecomendado.longitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'Longitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  //Puede mejorar diseño pero primero funcionalidad xd
  Widget _editActividad() {
    return Dismissible(
        key: Key(widget.lugarRecomendado.nombreLugar),
        background: Container(
          alignment: Alignment.centerLeft,
        ),
        child: ListTile(
            title: Text(
              "Estado:",
              style: TextStyle(fontSize: 18),
            ),
            trailing: CupertinoSwitch(
                value: widget.lugarRecomendado.estadoLugar,
                onChanged: (value) {
                  setState(() {
                    widget.lugarRecomendado.estadoLugar = value;
                    String idlugarRecomendado =
                        widget.lugarRecomendado.idlugarRecomendado.toString();
                    lugarRecomendadoService.patchLugarRecomendado(
                        idlugarRecomendado, widget.lugarRecomendado);
                  });
                })));
  }

  Widget _crearBoton(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        primary: Color.fromRGBO(88, 136, 220, 1.0),
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
      label: Text('Editar'),
      icon: Icon(Icons.save),
      onPressed: () {
        _submit(context);

        Navigator.pop(context);
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();
    final result = await lugarRecomendadoService.updateLugarRecomendado(
        widget.lugarRecomendado.idlugarRecomendado.toString(),
        widget.lugarRecomendado);
    if (result != null) {
      Fluttertoast.showToast(msg: "Se actualizó el lugar recomendado");
      Navigator.pop(context);
    }
  }
}
