import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/services/place_service.dart';

class PlaceEditWidget extends StatefulWidget {
  final Place lugarReferencia;
  const PlaceEditWidget({Key key, this.lugarReferencia}) : super(key: key);

  @override
  _PlaceEditWidgetState createState() => _PlaceEditWidgetState();
}

class _PlaceEditWidgetState extends State<PlaceEditWidget> {
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  final lugarReferenciaService = new PlaceService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Editar Lugar "),
          backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        ),
        body: Container(
            padding: EdgeInsets.all(10.0), child: _formulario(context)));
  }

  Widget _formulario(BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            _editNombre(),
            SizedBox(
              height: 5,
            ),
            _editLatitud(),
            SizedBox(
              height: 5,
            ),
            _editLongitud(),
            SizedBox(
              height: 5,
            ),
            _editActividad(),
            SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 30,
            ),
            _crearBoton(context),
          ],
        ));
  }

  Widget _editNombre() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Nombre:   "),
      initialValue: widget.lugarReferencia.nombreLugar,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarReferencia.nombreLugar = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Nombre es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editLatitud() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Latitud:   "),
      initialValue: widget.lugarReferencia.latitud.toString(),
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarReferencia.latitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'Latitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editLongitud() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Longitud:   "),
      initialValue: widget.lugarReferencia.longitud.toString(),
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.lugarReferencia.longitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'Longitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  //Puede mejorar diseño pero primero funcionalidad xd
  Widget _editActividad() {
    return Dismissible(
        key: Key(widget.lugarReferencia.nombreLugar),
        background: Container(
          alignment: Alignment.centerLeft,
        ),
        child: ListTile(
            title: Text(
              "Estado:",
              style: TextStyle(fontSize: 18),
            ),
            trailing: CupertinoSwitch(
                value: widget.lugarReferencia.estadoLugar,
                onChanged: (value) {
                  setState(() {
                    widget.lugarReferencia.estadoLugar = value;
                    String idlugarReferencia =
                        widget.lugarReferencia.idLugar.toString();
                    lugarReferenciaService.patchPlaces(
                        idlugarReferencia, widget.lugarReferencia);
                  });
                })));
  }

  Widget _crearBoton(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        primary: Color.fromRGBO(88, 136, 220, 1.0),
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
      label: Text('Editar'),
      icon: Icon(Icons.save),
      onPressed: () {
        _submit(context);

        Navigator.pop(context);
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();
    final result = await lugarReferenciaService.updatePlaces(
        widget.lugarReferencia.idLugar.toString(), widget.lugarReferencia);
    if (result != null) {
      Fluttertoast.showToast(msg: "Se actualizó el lugar");
      Navigator.pop(context);
    }
  }
}
