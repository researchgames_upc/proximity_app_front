import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/pages/admin_home_page.dart';
import 'package:proximity/src/pages/home_page.dart';
import 'package:proximity/src/services/user_service.dart';

class LoginWidget extends StatefulWidget {
  final Size size;
  const LoginWidget({
    Key key,
    this.size,
  }) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final formkey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  UserService userService = new UserService();
  String usuario;
  String password;

  @override
  Widget build(BuildContext context) {
    usuario = "";
    password = "";
    return Container(
      width: widget.size.width * 0.85,
      margin: EdgeInsets.symmetric(vertical: 30.0),
      padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 20.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black26,
                blurRadius: 3.0,
                offset: Offset(0.0, 5.0),
                spreadRadius: 3.0)
          ]),
      child: Form(
          key: formkey,
          child: Column(
            children: [
              TextFormField(
                initialValue: usuario,
                decoration: InputDecoration(hintText: "Ingrese e-mail"),
                keyboardType: TextInputType.emailAddress,
                onSaved: (value) => usuario = value.trim(),
              ),
              SizedBox(height: 10.0),
              TextFormField(
                initialValue: password,
                decoration: InputDecoration(hintText: "Password"),
                keyboardType: TextInputType.visiblePassword,
                onSaved: (value) => password = value.trim(),
                obscureText: true,
              ),
              SizedBox(height: 40.0),
              MaterialButton(
                  padding: EdgeInsets.all(15.0),
                  color: Color.fromRGBO(88, 136, 220, 1.0),
                  textColor: Colors.white,
                  child: Text("Ingresar"),
                  splashColor: Colors.amber,
                  onPressed: () {
                    // Navigator.pushReplacementNamed(context, 'home');
                    _submit(context);
                  })
            ],
          )),
    );
  }

  void _submit(BuildContext context) async {
    if (!formkey.currentState.validate()) {
      return;
    }
    formkey.currentState.save();
    final result = await userService.login(usuario, password);

    if (result.data != null) {
      //print(result);
      if (result.data.user.roles[0] == "user") {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HomePage(),
        ));
      } else {
        //print(result.data.user.roles);
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => AdminHomePage(),
        ));
      }
    } else {
      Fluttertoast.showToast(msg: result.message);
    }
  }
}
