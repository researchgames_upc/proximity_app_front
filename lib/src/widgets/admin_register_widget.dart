import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/user_model.dart';
import 'package:proximity/src/pages/login_page.dart';
import 'package:proximity/src/services/user_service.dart';

class AdminRegisterWidget extends StatefulWidget {
  @override
  _AdminRegisterWidgetState createState() => _AdminRegisterWidgetState();
}

class _AdminRegisterWidgetState extends State<AdminRegisterWidget> {
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();

  UserModel usuario = new UserModel();
  final userService = new UserService();

  @override
  Widget build(BuildContext context) {
    return _formulario(context);
  }

  Widget _formulario(BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            _crearNombre(),
            SizedBox(
              height: 10,
            ),
            _crearApellido(),
            SizedBox(
              height: 10,
            ),
            _crearEmail(),
            SizedBox(
              height: 10,
            ),
            _crearPwd(),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 30,
            ),
            _crearBoton(context)
          ],
        ));
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: "",
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nombre de Administrador'),
      onSaved: (value) => usuario.nombreUsuario = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Nombre es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearApellido() {
    return TextFormField(
      initialValue: "",
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Apellido de Administrador'),
      onSaved: (value) => usuario.apellidoUsuario = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Apellido es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearEmail() {
    return TextFormField(
      initialValue: "",
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(labelText: 'email'),
      onSaved: (value) => usuario.emailUsuario = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Email es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearPwd() {
    return TextFormField(
      initialValue: "",
      // keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      decoration: InputDecoration(labelText: 'Contraseña'),
      onSaved: (value) => usuario.passwordUsuario = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Contraseña es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearBoton(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        primary: Color.fromRGBO(88, 136, 220, 1.0),
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
      label: Text('Registrar'),
      icon: Icon(Icons.save),
      onPressed: () {
        usuario.roles = "admin";
        usuario.estado = false;
        _submit(context);
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();

    print(usuario);
    final result = await userService.nuevoUsuario(usuario);

    if (!result.error) {
      Fluttertoast.showToast(msg: "Se creó el usuario correctamente");
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => LoginPage(),
      ));
    } else {
      Fluttertoast.showToast(msg: "No se pudo registrar al administrador");
    }
  }
}
