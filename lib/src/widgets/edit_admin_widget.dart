import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/login_response.dart';
import 'package:proximity/src/services/user_service.dart';

class AdminEditWidget extends StatefulWidget {
  final User admin;
  const AdminEditWidget({Key key, this.admin}) : super(key: key);

  @override
  _AdminEditWidgetState createState() => _AdminEditWidgetState();
}

class _AdminEditWidgetState extends State<AdminEditWidget> {
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  final userService = new UserService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Editar Administrador"),
          backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        ),
        body: Container(
            padding: EdgeInsets.all(10.0), child: _formulario(context)));
  }

  Widget _formulario(BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            _editNombre(),
            SizedBox(
              height: 5,
            ),
            _editApellido(),
            SizedBox(
              height: 5,
            ),
            _editRol(),
            SizedBox(
              height: 5,
            ),
            _editActividad(),
            SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 30,
            ),
            _crearBoton(context),
          ],
        ));
  }

  Widget _editNombre() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Nombres:   "),
      initialValue: widget.admin.nombreUsuario,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.admin.nombreUsuario = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Nombre es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editApellido() {
    return TextFormField(
      decoration: InputDecoration(prefixText: "Apellidos:   "),
      initialValue: widget.admin.apellidoUsuario,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => widget.admin.apellidoUsuario = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Appellido es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _editRol() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Text(
        'Rol:  ${widget.admin.roles.toString()}',
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: 18,
        ),
      ),
    );
  }

  //Puede mejorar diseño pero primero funcionalidad xd
  Widget _editActividad() {
    return Dismissible(
        key: Key(widget.admin.nombreUsuario),
        background: Container(
          alignment: Alignment.centerLeft,
        ),
        child: ListTile(
            title: Text(
              "Estado:",
              style: TextStyle(fontSize: 18),
            ),
            trailing: CupertinoSwitch(
                value: widget.admin.estado,
                onChanged: (value) {
                  setState(() {
                    widget.admin.estado = value;
                    String idAdmin = widget.admin.idUsuario.toString();
                    userService.patchUser(idAdmin, widget.admin);
                  });
                })));
  }

  Widget _crearBoton(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        primary: Color.fromRGBO(88, 136, 220, 1.0),
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
      label: Text('Editar'),
      icon: Icon(Icons.save),
      onPressed: () {
        _submit(context);
        /* de esta forma aun si se ponen mal los datos ( osea se deja vacio el nombre o apellido)
        regresa al menu de admins, voy a buscar una mejor forma de implementarlo, pero ya 
        todo funciona y hace refresh 
       */
        Navigator.pop(context);
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();
    final result = await userService.updateUser(
        widget.admin.idUsuario.toString(), widget.admin);
    if (result != null) {
      Fluttertoast.showToast(msg: "Se actualizó el administrador");
    }
  }
}
