import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/recomended_place_model.dart';
import 'package:proximity/src/services/recomended_place_service.dart';

class AddPlaceRecomendedWidget extends StatefulWidget {
  @override
  _AddPlaceRecomendedWidgetState createState() =>
      _AddPlaceRecomendedWidgetState();
}

class _AddPlaceRecomendedWidgetState extends State<AddPlaceRecomendedWidget> {
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();

  bool checkBoxValue = false;
  RecomendedPlace lugarRecomendado = new RecomendedPlace();
  final lugarRecomendadoService = new RecomendedPlaceService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Registro de Recomendaciones"),
          backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        ),
        body: Container(
            padding: EdgeInsets.all(20.0), child: _formulario(context)));
  }

  Widget _formulario(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              _crearNombre(),
              SizedBox(
                height: 10,
              ),
              _crearLatitud(),
              SizedBox(
                height: 10,
              ),
              _crearLongitud(),
              SizedBox(
                height: 10,
              ),
              _crearActivo(),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 10,
              ),
              _crearDescripcion(),
              SizedBox(
                height: 10,
              ),
              _crearImagen(),
              SizedBox(
                height: 10,
              ),
              _crearBoton(context)
            ],
          )),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nombre del Lugar Recomendado'),
      onSaved: (value) => lugarRecomendado.nombreLugar = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Nombre es requerido';
        }
        if (value.length < 3) {
          return 'minimo 6 caracteres';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearDescripcion() {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Descripción del Recomendado'),
      onSaved: (value) => lugarRecomendado.descripcion = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Descripción es requerida';
        }
        if (value.length < 3) {
          return 'minimo 6 caracteres';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearImagen() {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Link de imagen'),
      onSaved: (value) => lugarRecomendado.imagen = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Link de imagen es requerido';
        }
        if (value.length < 3) {
          return 'minimo 6 caracteres';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearLatitud() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(labelText: 'Latitud'),
      onSaved: (value) => lugarRecomendado.latitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'latitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearLongitud() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(labelText: 'Longitud'),
      onSaved: (value) => lugarRecomendado.longitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'Longitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearActivo() {
    return Row(
      children: [
        Text("Activo :"),
        Checkbox(
          value: checkBoxValue,
          onChanged: (bool value) {
            print(value);
            setState(() {
              checkBoxValue = value;
            });
          },
        ),
      ],
    );
  }

  Widget _crearBoton(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        primary: Color.fromRGBO(0, 70, 161, 1),
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
      label: Text('Registrar'),
      icon: Icon(Icons.save),
      onPressed: () {
        _submit(context);
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();
    lugarRecomendado.estadoLugar = checkBoxValue;
    print(lugarRecomendado);
    final result =
        await lugarRecomendadoService.nuevoLugarRecomendado(lugarRecomendado);
    print("al grabar : ");
    print(result);
    if (result == null) {
      Navigator.pop(context);
      Fluttertoast.showToast(msg: "Se registró el lugar recomendado");
    }
  }
}
