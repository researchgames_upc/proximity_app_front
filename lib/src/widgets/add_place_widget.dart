import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/services/place_service.dart';

class AddPlaceWidget extends StatefulWidget {
  @override
  _AddPlaceWidgetState createState() => _AddPlaceWidgetState();
}

class _AddPlaceWidgetState extends State<AddPlaceWidget> {
  final formKey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();

  bool checkBoxValue = false;
  Place lugarReferencia = new Place();
  final lugarReferenciaService = new PlaceService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Registro de Referencias"),
          backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        ),
        body: Container(
            padding: EdgeInsets.all(10.0), child: _formulario(context)));
  }

  Widget _formulario(BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            _crearNombre(),
            SizedBox(
              height: 10,
            ),
            _crearLatitud(),
            SizedBox(
              height: 10,
            ),
            _crearLongitud(),
            SizedBox(
              height: 10,
            ),
            _crearActivo(),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 30,
            ),
            _crearBoton(context)
          ],
        ));
  }

  Widget _crearNombre() {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nombre del Lugar de referencia '),
      onSaved: (value) => lugarReferencia.nombreLugar = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'Nombre del Lugar es requerido';
        }
        if (value.length < 3) {
          return 'minimo 6 caracteres';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearLatitud() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(labelText: 'Latitud'),
      onSaved: (value) => lugarReferencia.latitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'latitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearLongitud() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(labelText: 'Longitud'),
      onSaved: (value) => lugarReferencia.longitud = num.parse(value),
      validator: (value) {
        if (value.isEmpty) {
          return 'Longitud es requerido';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearActivo() {
    return Row(
      children: [
        Text("Activo :"),
        Checkbox(
          value: checkBoxValue,
          onChanged: (bool value) {
            print(value);
            setState(() {
              checkBoxValue = value;
            });
          },
        ),
      ],
    );
  }

  Widget _crearBoton(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        primary: Color.fromRGBO(0, 70, 161, 1),
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
      label: Text('Registrar'),
      icon: Icon(Icons.save),
      onPressed: () {
        _submit(context);
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();
    lugarReferencia.estadoLugar = checkBoxValue;
    print(lugarReferencia);
    final result = await lugarReferenciaService.createPlaces(lugarReferencia);
    if (result != null) {
      Fluttertoast.showToast(msg: "Se registró el lugar");
      Navigator.pop(context);
    }
  }
}
