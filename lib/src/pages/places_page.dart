import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/services/place_service.dart';
import 'package:proximity/src/widgets/add_place_widget.dart';
import 'package:proximity/src/widgets/edit_place_widget.dart';

class PlacesPage extends StatefulWidget {
  @override
  _PlacesPageState createState() => _PlacesPageState();
}

class _PlacesPageState extends State<PlacesPage> {
  PlaceService placeService = new PlaceService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Lugares Habilitados"),
        backgroundColor: Color.fromRGBO(89, 136, 220, 1.0),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: FutureBuilder(
            future: placeService.getPlaces(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              print("estado: " + snapshot.connectionState.toString());
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                print("lista de lugares");
                final List<Place> listalugares = snapshot.data;
                print(listalugares);
                return _Place(listalugares);
              }
            }),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(0, 70, 161, 1),
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
                builder: (context) => AddPlaceWidget(),
              ))
              .then((value) => setState(() {}));
        },
      ),
      /*  */
    );
  }
}

class _Place extends StatefulWidget {
  final List<Place> listaLugares;
  const _Place(this.listaLugares);
  @override
  __PlaceState createState() => __PlaceState();
}

class __PlaceState extends State<_Place> {
  PlaceService placeService = new PlaceService();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.listaLugares.length,
      itemBuilder: (BuildContext context, int index) {
        final lugarReferencia = widget.listaLugares[index];
        return Dismissible(
          key: UniqueKey(),
          onDismissed: (DismissDirection direction) async {
            final info = await placeService
                .deletePlaces(widget.listaLugares[index].idLugar.toString());
            if (info != null) {
              Fluttertoast.showToast(msg: "Se eliminó el lugar");
            }
          },
          background: Container(
            child: Icon(Icons.delete),
            color: Colors.red[900],
          ),
          child: ListTile(
            contentPadding: EdgeInsets.all(12),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      PlaceEditWidget(lugarReferencia: lugarReferencia),
                ),
              ).then((value) => setState(() {}));
            },
            leading: Icon(Icons.place),
            title: Text(lugarReferencia.nombreLugar),
            subtitle: Text('Latitud:' +
                lugarReferencia.latitud.toString() +
                ' Longitud:' +
                lugarReferencia.longitud.toString()),
            trailing: CupertinoSwitch(
              value: lugarReferencia.estadoLugar,
              onChanged: (value) {
                setState(() {
                  lugarReferencia.estadoLugar = value;
                  String idLugarReomendado = lugarReferencia.idLugar.toString();
                  placeService.patchPlaces(idLugarReomendado, lugarReferencia);
                });
              },
            ),
          ),
        );
      },
    );
  }
}
