import 'package:flutter/material.dart';
import 'package:proximity/src/widgets/login_widget.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;

  @override
  void initState() {
    super.initState();
    _iconAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _iconAnimation = CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.bounceInOut);

    _iconAnimation.addListener(() {
      this.setState(() {});
    });
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final iconApp = Container(
      child: Image(
        image: AssetImage("assets/logo.png"),
        fit: BoxFit.contain,
        color: Colors.white,
        width: size.width * 0.5 * _iconAnimation.value,
        height: size.width * 0.3 * _iconAnimation.value,
      ),
    );

    return Container(
      child: Scaffold(
        backgroundColor: Colors.blue[900],
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image(
              image: AssetImage("assets/mapa.png"),
              fit: BoxFit.contain,
              color: Color.fromRGBO(88, 136, 220, 1.0),
              //colorBlendMode: BlendMode.colorDodge,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                iconApp,
                LoginWidget(size: size),
                TextButton(
                  child: Text(
                    "Crear una Cuenta",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, 'register');
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
