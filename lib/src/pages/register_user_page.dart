import 'package:flutter/material.dart';
import 'package:proximity/src/widgets/admin_register_widget.dart';
import 'package:proximity/src/widgets/user_register_widget.dart';

class RegisterUserPage extends StatefulWidget {
  @override
  _RegisterUserPageState createState() => _RegisterUserPageState();
}

class _RegisterUserPageState extends State<RegisterUserPage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Registro"),
          backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
          bottom: const TabBar(tabs: [
            Tab(text: "User"),
            Tab(text: "Admin"),
          ]),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(30),
            padding: EdgeInsets.all(15),
            height: size.height * 0.6,
            width: size.width * 0.9,
            child: TabBarView(
                children: [UserRegisterWidget(), AdminRegisterWidget()]),
          ),
        ),
      ),
    );
  }
}
