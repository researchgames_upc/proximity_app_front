import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proximity/src/models/recomended_place_model.dart';
import 'package:proximity/src/services/recomended_place_service.dart';
import 'package:proximity/src/widgets/add_place_recomended_widget.dart';
import 'package:proximity/src/widgets/edit_place_recomended_widget.dart';

class RecomendedPlacesPage extends StatefulWidget {
  @override
  _RecomendedPlacesPageState createState() => _RecomendedPlacesPageState();
}

class _RecomendedPlacesPageState extends State<RecomendedPlacesPage> {
  RecomendedPlaceService recomendedPlaceService = new RecomendedPlaceService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Lugares Recomendados"),
        backgroundColor: Color.fromRGBO(89, 136, 220, 1.0),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: FutureBuilder(
            future: recomendedPlaceService.getRecomendedPlaces(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                final List<RecomendedPlace> listalugaresRecomendados =
                    snapshot.data;
                return _RecomendedPlace(listalugaresRecomendados);
              }
            }),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(0, 70, 161, 1),
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
                builder: (context) => AddPlaceRecomendedWidget(),
              ))
              .then((value) => setState(() {}));
        },
      ),
    );
  }
}

class _RecomendedPlace extends StatefulWidget {
  final List<RecomendedPlace> listalugaresRecomendados;
  const _RecomendedPlace(this.listalugaresRecomendados);
  @override
  __RecomendedPlaceState createState() => __RecomendedPlaceState();
}

class __RecomendedPlaceState extends State<_RecomendedPlace> {
  RecomendedPlaceService recomendedPlaceService = new RecomendedPlaceService();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.listalugaresRecomendados.length,
      itemBuilder: (BuildContext context, int index) {
        final recomendedPlace = widget.listalugaresRecomendados[index];
        return Dismissible(
          key: UniqueKey(),
          /* Error en el handler de este onDismissed
          cuando eliminar y intentas crear otro lugar,
          si colocas un setState ya no sale el error
          pero no hace refresh a la lista, por lo que 1) se podría implementar un refresh o 
          2) manejar bien el handler */
          onDismissed: (DismissDirection direction) async {
            print("eliminando: " +
                widget.listalugaresRecomendados[index].idlugarRecomendado
                    .toString());
            final info = await recomendedPlaceService.deleteLugarRecomendado(
                widget.listalugaresRecomendados[index].idlugarRecomendado
                    .toString());

            print(info.toString());
            setState(() {
              widget.listalugaresRecomendados.removeAt(index);
              Fluttertoast.showToast(msg: "Se eliminó el lugar recomendado");
            });
            //if (info == null) {
            //}
          },
          background: Container(
            child: Icon(Icons.delete),
            color: Colors.red[900],
          ),
          child: ListTile(
            contentPadding: EdgeInsets.all(12),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RecomendedPlaceEditWidget(
                      lugarRecomendado: recomendedPlace),
                ),
              ).then((value) => setState(() {}));
            },
            leading: Icon(Icons.place),
            title: Text(recomendedPlace.nombreLugar),
            subtitle: Text('Latitud:' +
                recomendedPlace.latitud.toString() +
                ' Longitud:' +
                recomendedPlace.longitud.toString()),
            trailing: CupertinoSwitch(
              value: recomendedPlace.estadoLugar,
              onChanged: (value) {
                setState(() {
                  recomendedPlace.estadoLugar = value;
                  String idLugarReomendado =
                      recomendedPlace.idlugarRecomendado.toString();
                  recomendedPlaceService.patchLugarRecomendado(
                      idLugarReomendado, recomendedPlace);
                });
              },
            ),
          ),
        );
      },
    );
  }
}
