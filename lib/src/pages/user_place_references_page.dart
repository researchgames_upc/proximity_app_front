import 'package:flutter/material.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/services/place_service.dart';

class PlaceReferencesUserPage extends StatefulWidget {
  @override
  _PlaceReferencesUserPageState createState() =>
      _PlaceReferencesUserPageState();
}

class _PlaceReferencesUserPageState extends State<PlaceReferencesUserPage> {
  PlaceService placeService = new PlaceService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lugares Habilitados"),
        backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: FutureBuilder(
            future: placeService.getPlaces(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                final List<Place> listalugares = snapshot.data;
                return _Place(listalugares);
              }
            }),
      ),
    );
  }
}

class _Place extends StatefulWidget {
  final List<Place> listaLugares;
  const _Place(this.listaLugares);
  @override
  __PlaceState createState() => __PlaceState();
}

class __PlaceState extends State<_Place> {
  PlaceService placeService = new PlaceService();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.listaLugares.length,
      itemBuilder: (BuildContext context, int index) {
        final lugarReferencia = widget.listaLugares[index];
        return ListTile(
          contentPadding: EdgeInsets.all(12),
          leading: Icon(Icons.place),
          title: Text(lugarReferencia.nombreLugar),
          subtitle: Text('Latitud:' +
              lugarReferencia.latitud.toString() +
              ' Longitud:' +
              lugarReferencia.longitud.toString()),
        );
      },
    );
  }
}
