import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proximity/src/models/login_response.dart';
import 'package:proximity/src/services/user_service.dart';
import 'package:proximity/src/widgets/edit_admin_widget.dart';

class AdminsPage extends StatefulWidget {
  @override
  _AdminsPageState createState() => _AdminsPageState();
}

class _AdminsPageState extends State<AdminsPage> {
  UserService userService = new UserService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Administradores"),
        backgroundColor: Color.fromRGBO(89, 136, 220, 1.0),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: FutureBuilder(
            future: userService.getAdmins(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                final List<User> listaAdmins = snapshot.data;
                print(listaAdmins);
                return _Admin(listaAdmins);
              }
            }),
      ),

      /*  */
    );
  }
}

class _Admin extends StatefulWidget {
  final List<User> listaAdmins;
  const _Admin(this.listaAdmins);
  @override
  __AdminState createState() => __AdminState();
}

class __AdminState extends State<_Admin> {
  UserService userService = new UserService();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      
      itemCount: widget.listaAdmins.length,
      itemBuilder: (BuildContext context, int index) {
        final admin = widget.listaAdmins[index];
        return Dismissible(
            key: Key(widget.listaAdmins[index].nombreUsuario),
            /* onDismissed: (DismissDirection direction) async {
            final info = await placeService.deletePet(widget.mascotas[index]);

            if (!info.error) {
              setState(() {
                widget.mascotas.removeAt(index);
                Fluttertoast.showToast(msg: "Mascota Eliminada");
              });
            } else {
              Fluttertoast.showToast(msg: info.errorMessage);
            }
          }, */
            background: Container(
              child: Icon(Icons.delete),
              color: Colors.red[900],
            ),
          
            child: ListTile(
                contentPadding: EdgeInsets.all(12),
               onTap: (){
                  Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AdminEditWidget(admin: admin),
          ),).then((value) => setState(() {}));
               },
                leading: Icon(Icons.person),
                title: Text(admin.emailUsuario),
                subtitle: Text(admin.roles[0]),
                trailing: CupertinoSwitch(
                    value: admin.estado,
                    onChanged: (value) {
                      setState(() {
                        admin.estado = value;
                        String idAdmin = admin.idUsuario.toString();
                        userService.patchUser(idAdmin, admin);
                      });
                    })));
      },
    );
  }
}


/* Widget _element(BuildContext context, User admin) {
  UserService userService = new UserService();
  return ListTile(
      contentPadding: EdgeInsets.all(12),
      leading: Icon(Icons.person),
      title: Text(admin.nombreUsuario),
      subtitle: Text(admin.roles[0]),
      trailing:
          /* IconButton(
      icon: Icon(
        Icons.security,
        color: Color.fromRGBO(89, 136, 220, 1.0),
      ),
      onPressed: () {
        // desactivar/activar
        var state = admin.estado;
        state ? admin.estado = false : admin.estado = true;
        print("usuario actualizado: " + admin.toString());
        String idAdmin = admin.idUsuario.toString();
        var info = userService.patchUser(idAdmin, admin);
        if (info != null) {
          userService.getAdmins();
        }
      },
    ), */
          CupertinoSwitch(
              value: admin.estado,
              onChanged: (value) {
                admin.estado = value;
                String idAdmin = admin.idUsuario.toString();
                userService.patchUser(idAdmin, admin);
              }));
} */
