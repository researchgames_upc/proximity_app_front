import 'package:flutter/material.dart';
import 'package:proximity/src/utils/preferencias_usuario.dart';

class PerfilAdminPage extends StatefulWidget {
  @override
  _PerfilUserPageState createState() => _PerfilUserPageState();
}

class _PerfilUserPageState extends State<PerfilAdminPage> {
  final _pref = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Perfil Admin"),
        backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: ElevatedButton(
            child: Text("Cerrar Session"),
            onPressed: () {
              _pref.initPrefs();
              Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
            },
            style: ElevatedButton.styleFrom(
              primary: Color.fromRGBO(0, 70, 161, 1),
            )),
      ),
    );
  }
}
