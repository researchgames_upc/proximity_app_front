import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:proximity/src/pages/perfil_user_page.dart';
import 'package:proximity/src/pages/show_place_page.dart';
import 'package:proximity/src/pages/user_place_references_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _NavegacionModel(),
      child: Scaffold(
        body: _Paginas(),
        bottomNavigationBar: _Navegacion(),
      ),
    );
  }
}

class _Navegacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModel>(context);

    return BottomNavigationBar(
        currentIndex: navegacionModel.paginaActual,
        onTap: (i) => navegacionModel.paginaActual = i,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Inicio",
            activeIcon: Icon(Icons.home_outlined),
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.place),
            label: "Ubicar",
            activeIcon: Icon(Icons.place_outlined),
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ),
          /* BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Perfil",
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ), */
        ]);
  }
}

class _Paginas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModel>(context);
    // final _pref = new PreferenciasUsuario();
    return PageView(
      //physics: BouncingScrollPhysics(),
      controller: navegacionModel.pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        PerfilUserPage(),
        ShowPlacePage(),
        /* PlaceReferencesUserPage(), */
      ],
    );
  }
}

class _NavegacionModel with ChangeNotifier {
  int _paginaActual = 0;

  PageController _pageController = new PageController();

  int get paginaActual => this._paginaActual;
  set paginaActual(int valor) {
    this._paginaActual = valor;
    _pageController.animateToPage(valor,
        duration: Duration(milliseconds: 250), curve: Curves.easeOut);

    notifyListeners();
  }

  PageController get pageController => this._pageController;
}
