import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import "package:latlong/latlong.dart";
import 'package:proximity/src/models/marker_response.dart';

class FullScreenMap extends StatelessWidget {
  final PopupController _popupLayerController = PopupController();
  num longitud;
  num latitud;
  final List<Marcador> info;
  FullScreenMap(this.latitud, this.longitud, this.info);

  @override
  Widget build(BuildContext context) {
    List<Marker> _markers = <Marker>[];
    _markers.add(Marker(
        point: new LatLng(latitud, longitud),
        builder: (ctx) => new Container(
                child: IconButton(
              icon: Icon(Icons.person),
              color: Colors.blue,
              iconSize: 50.0,
              onPressed: () {},
            ))));
    info.forEach((element) {
      _markers.add(MarcadorMarker(
          marcador: Marcador(
              nombreLugar: element.nombreLugar,
              imagen: element.imagen,
              latitud: element.latitud,
              longitud: element.longitud,
              descripcion: element.descripcion)));
    });
    return new Scaffold(
        appBar: new AppBar(
            title: Text("Maps"),
            backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
            automaticallyImplyLeading: false),
        body: new FlutterMap(
            options: new MapOptions(
              center: new LatLng(latitud, longitud),
              minZoom: 16.5,
              interactiveFlags: InteractiveFlag.all,
              onTap: (_) => _popupLayerController.hidePopup(),
            ),
            children: [
              TileLayerWidget(
                options: TileLayerOptions(
                  urlTemplate:
                      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                  subdomains: ['a', 'b', 'c'],
                ),
              ),
              CircleLayerWidget(
                  options: CircleLayerOptions(circles: [
                CircleMarker(
                    point: LatLng(latitud, longitud),
                    color: Colors.blue.withOpacity(0.1),
                    borderStrokeWidth: 2,
                    useRadiusInMeter: true,
                    radius: 500)
              ])),
              PopupMarkerLayerWidget(
                  options: PopupMarkerLayerOptions(
                      popupController: _popupLayerController,
                      markers: _markers,
                      popupBuilder: (_, Marker marker) {
                        if (marker is MarcadorMarker) {
                          return MarcadorMarkerPopUp(marcador: marker.marcador);
                        }
                        return Card(child: const Text('No es un marcador'));
                      }))
            ]));
  }
}

class MarcadorMarker extends Marker {
  MarcadorMarker({this.marcador})
      : super(
          anchorPos: AnchorPos.align(AnchorAlign.top),
          height: Marcador.size,
          width: Marcador.size,
          point: LatLng(marcador.latitud, marcador.longitud),
          builder: (_) => Icon(
            Icons.location_on,
            size: 40,
            color: Colors.red,
          ),
        );
  final Marcador marcador;
}

class MarcadorMarkerPopUp extends StatelessWidget {
  const MarcadorMarkerPopUp({Key key, this.marcador}) : super(key: key);
  final Marcador marcador;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.network(marcador.imagen, width: 200),
            Text(marcador.nombreLugar),
            Text(marcador.descripcion),
          ],
        ),
      ),
    );
  }
}
