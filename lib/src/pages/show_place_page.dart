import 'dart:io';
import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';

import 'package:proximity/src/models/marker_response.dart';
import 'package:proximity/src/models/place_model.dart';
import 'package:proximity/src/pages/fullscreenmap.dart';
import 'package:proximity/src/services/recomended_place_service.dart';

class ShowPlacePage extends StatefulWidget {
  @override
  _ShowPlacePageState createState() => _ShowPlacePageState();
}

class _ShowPlacePageState extends State<ShowPlacePage> {
  String imagePath;
  Dio dio = new Dio();
  Place lugarObtenido = new Place();
  RecomendedPlaceService recomendedPlaceService = new RecomendedPlaceService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Reconocedor de Lugares"),
        backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: [
              Text(
                  "Captura una foto con este celular y esta aplicación te mostrará el lugar en el que te encuentras, además te recomendaremos lugares inéditos cercanos"),
              /*  SizedBox(
                height: 10,
              ),
              Text(
                lugarObtenido != null ? lugarObtenido.nombreLugar : "",
                style: TextStyle(
                    color: Colors.black87,
                    decoration: TextDecoration.underline,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ), */
              SizedBox(
                height: 10,
              ),
              (imagePath == null) ? Container() : Image.file(File(imagePath)),
              SizedBox(
                height: 5,
              ),
              ElevatedButton.icon(
                icon: Icon(Icons.camera),
                style: ElevatedButton.styleFrom(
                  alignment: Alignment.centerLeft,
                  primary: Color.fromRGBO(0, 70, 161, 1),
                ),
                label: Text("Capturar"),
                onPressed: () async {
                  lugarObtenido = null;
                  final ImagePicker _picker = ImagePicker();
                  XFile _pickedFile =
                      await _picker.pickImage(source: ImageSource.gallery);
                  setState(() {
                    imagePath = _pickedFile.path;
                    print(imagePath);
                  });
                  try {
                    /*  _pickedFile.readAsBytes().then((value) {
                    //llamar al api
                    }); */
                    String filename = imagePath.split('/').last;
                    FormData formData = new FormData.fromMap({
                      "image": await MultipartFile.fromFile(imagePath,
                          filename: filename,
                          contentType: new MediaType('image', 'jpg')),
                      "type": "image/jpg"
                    });

                    this.lugarObtenido =
                        await recomendedPlaceService.getPositionPlace(formData);
                  } catch (e) {
                    print(e);
                  }

                  /*  en descripción como prueba puedes poner los m de distancia para que veas que 
                      funciona xd, puede haber un error de +-20 m ,ya que el SRID con el que estamos
                      trabajando es el 3857. */
                },
              ),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(
                  onPressed: () async {
                    var range = 1000;
                    /*este es el rango que asignas para obtener un una lista en json 
                      con todos los lugares que esten dentro de este. el servicio devuelve una lista de json
                      */

                    List<Marcador> info = await recomendedPlaceService
                        .getMarkers(range.toString(), lugarObtenido);

                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => FullScreenMap(
                          lugarObtenido.latitud, lugarObtenido.longitud, info),
                    ));
                  },
                  style: ElevatedButton.styleFrom(
                    alignment: Alignment.centerLeft,
                    primary: Color.fromRGBO(0, 70, 161, 1),
                  ),
                  child: Text("Aceptar"))
            ],
          ),
        ),
      ),
    );
  }
}
