import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:proximity/src/pages/admins_page.dart';
import 'package:proximity/src/pages/perfil_admin_page.dart';
import 'package:proximity/src/pages/places_page.dart';
import 'package:proximity/src/pages/recomended_places_page.dart';

class AdminHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _NavegacionModel(),
      child: Scaffold(
        body: _Paginas(),
        bottomNavigationBar: _Navegacion(),
      ),
    );
  }
}

class _Navegacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModel>(context);

    return BottomNavigationBar(
        currentIndex: navegacionModel.paginaActual,
        onTap: (i) => navegacionModel.paginaActual = i,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.place),
            label: "Lugares",
            activeIcon: Icon(Icons.place_outlined),
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: "Recomendaciones",
            activeIcon: Icon(Icons.favorite_border_outlined),
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.admin_panel_settings),
            label: "Administradores",
            activeIcon: Icon(Icons.admin_panel_settings_outlined),
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Perfil",
            activeIcon: Icon(Icons.person_outline),
            backgroundColor: Color.fromRGBO(0, 70, 161, 1),
          ),
        ]);
  }
}

class _Paginas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModel>(context);
    //final _pref = new PreferenciasUsuario();
    return PageView(
      //physics: BouncingScrollPhysics(),
      controller: navegacionModel.pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        PlacesPage(),
        RecomendedPlacesPage(),
        AdminsPage(),
        PerfilAdminPage(),
      ],
    );
  }
}

class _NavegacionModel with ChangeNotifier {
  int _paginaActual = 0;

  PageController _pageController = new PageController();

  int get paginaActual => this._paginaActual;
  set paginaActual(int valor) {
    this._paginaActual = valor;
    _pageController.animateToPage(valor,
        duration: Duration(milliseconds: 250), curve: Curves.easeOut);

    notifyListeners();
  }

  PageController get pageController => this._pageController;
}
