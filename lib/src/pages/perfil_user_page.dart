import 'package:flutter/material.dart';
import 'package:proximity/src/models/login_response.dart';
import 'package:proximity/src/services/user_service.dart';
import 'package:proximity/src/utils/preferencias_usuario.dart';

class PerfilUserPage extends StatefulWidget {
  @override
  _PerfilUserPageState createState() => _PerfilUserPageState();
}

class _PerfilUserPageState extends State<PerfilUserPage> {
  final _pref = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Perfil usuario"),
        backgroundColor: Color.fromRGBO(88, 136, 220, 1.0),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 40,
              ),
              Text(
                "Bienvenido " + _pref.nombreUser,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(0, 70, 161, 1)),
              ),
              SizedBox(
                height: 60,
              ),
              ElevatedButton(
                child: Text("Cerrar Session"),
                onPressed: () {
                  _pref.initPrefs();
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/', (route) => false);
                },
                style: ElevatedButton.styleFrom(
                  primary: Color.fromRGBO(0, 70, 161, 1),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
