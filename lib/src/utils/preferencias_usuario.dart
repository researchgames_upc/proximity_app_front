import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  get endPoint {
    //return _prefs.getString('urlPetcare') ?? "https://10.0.2.2:5001/api";
    return _prefs.getString('url') ?? "http://10.0.2.2:8080";
  }

  set endPoint(String value) {
    _prefs.setString('endPoint', value);
  }

  // GET y SET del nombre
  get token {
    return _prefs.getString('token');
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  get iduser {
    return _prefs.getInt('iduser');
  }

  set iduser(int value) {
    _prefs.setInt('iduser', value);
  }

  get nombreUser {
    return _prefs.getString('nombreUsuario');
  }

  set nombreUser(String value) {
    _prefs.setString('nombreUsuario', value);
  }

  setString(String s, String path) {}

  // GET y SET de la última página
  /* get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? 'login';
  }

  set ultimaPagina( String value ) {
    _prefs.setString('ultimaPagina', value);
  } */

}
