import 'package:flutter/material.dart';
import 'package:proximity/src/pages/home_page.dart';
import 'package:proximity/src/pages/login_page.dart';
import 'package:proximity/src/pages/recomended_places_page.dart';
import 'package:proximity/src/pages/register_user_page.dart';
import 'package:proximity/src/pages/show_place_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => LoginPage(),
    'register': (BuildContext context) => RegisterUserPage(),
    'home': (BuildContext context) => HomePage(),
    'place': (BuildContext context) => ShowPlacePage(),
    'listplacesr': (BuildContext context) => RecomendedPlacesPage(),
    /*  'result': (BuildContext context) => FullScreenMap(), */
  };
}
