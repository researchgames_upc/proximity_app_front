// To parse this JSON data, do
//
//     final userResponse = userResponseFromJson(jsonString);

import 'dart:convert';

UserResponse userResponseFromJson(String str) =>
    UserResponse.fromJson(json.decode(str));

String userResponseToJson(UserResponse data) => json.encode(data.toJson());

class UserResponse {
  UserResponse({
    this.message,
    this.data,
  });

  String message;
  Data data;

  factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.idUsuario,
    this.nombreUsuario,
    this.apellidoUsuario,
    this.emailUsuario,
    this.estado,
    this.roles,
    this.passwordUsuario,
  });

  int idUsuario;
  String nombreUsuario;
  String apellidoUsuario;
  String emailUsuario;
  bool estado;
  List<String> roles;
  String passwordUsuario;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        idUsuario: json["idUsuario"],
        nombreUsuario: json["nombreUsuario"],
        apellidoUsuario: json["apellidoUsuario"],
        emailUsuario: json["emailUsuario"],
        estado: json["estado"],
        roles: List<String>.from(json["roles"].map((x) => x)),
        passwordUsuario: json["passwordUsuario"],
      );

  Map<String, dynamic> toJson() => {
        "idUsuario": idUsuario,
        "nombreUsuario": nombreUsuario,
        "apellidoUsuario": apellidoUsuario,
        "emailUsuario": emailUsuario,
        "estado": estado,
        "roles": List<dynamic>.from(roles.map((x) => x)),
        "passwordUsuario": passwordUsuario,
      };
}
