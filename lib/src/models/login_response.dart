// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) =>
    LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.message,
    this.data,
  });

  String message;
  Data data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.user,
    this.accessToken,
  });

  User user;
  String accessToken;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        user: User.fromJson(json["user"]),
        accessToken: json["accessToken"],
      );

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
        "accessToken": accessToken,
      };
}

class User {
  User({
    this.idUsuario,
    this.nombreUsuario,
    this.apellidoUsuario,
    this.emailUsuario,
    this.estado,
    this.roles,
  });

  int idUsuario;
  String nombreUsuario;
  String apellidoUsuario;
  String emailUsuario;
  bool estado;
  List<String> roles;

  factory User.fromJson(Map<String, dynamic> json) => User(
        idUsuario: json["idUsuario"],
        nombreUsuario: json["nombreUsuario"],
        apellidoUsuario: json["apellidoUsuario"],
        emailUsuario: json["emailUsuario"],
        estado: json["estado"],
        roles: List<String>.from(json["roles"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "idUsuario": idUsuario,
        "nombreUsuario": nombreUsuario,
        "apellidoUsuario": apellidoUsuario,
        "emailUsuario": emailUsuario,
        "estado": estado,
        "roles": List<dynamic>.from(roles.map((x) => x)),
      };
}
