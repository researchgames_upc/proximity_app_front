// To parse this JSON data, do
//
//     final recomendedPlace = recomendedPlaceFromJson(jsonString);

import 'dart:convert';

RecomendedPlace recomendedPlaceFromJson(String str) =>
    RecomendedPlace.fromJson(json.decode(str));

String recomendedPlaceToJson(RecomendedPlace data) =>
    json.encode(data.toJson());

class RecomendedPlace {
  RecomendedPlace({
    this.idlugarRecomendado,
    this.nombreLugar,
    this.estadoLugar,
    this.latitud,
    this.longitud,
    this.descripcion,
    this.imagen,
    this.location,
  });

  int idlugarRecomendado;
  String nombreLugar;
  bool estadoLugar;
  num latitud;
  num longitud;
  String descripcion;
  String imagen;
  String location;

  factory RecomendedPlace.fromJson(Map<String, dynamic> json) =>
      RecomendedPlace(
        idlugarRecomendado: json["idLugarRecomendado"],
        nombreLugar: json["nombreLugar"],
        estadoLugar: json["estadoLugar"],
        latitud: json["latitud"],
        longitud: json["longitud"],
        descripcion: json["descripcion"],
        imagen: json["imagen"],
        location: json["location"],
      );

  Map<String, dynamic> toJson() => {
        "idLugarRecomendado": idlugarRecomendado,
        "nombreLugar": nombreLugar,
        "estadoLugar": estadoLugar,
        "latitud": latitud,
        "longitud": longitud,
        "descripcion": descripcion,
        "imagen": imagen,
        "location": location,
      };
}
