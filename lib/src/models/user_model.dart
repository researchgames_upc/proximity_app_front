// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.nombreUsuario,
    this.apellidoUsuario,
    this.emailUsuario,
    this.passwordUsuario,
    this.estado,
    this.roles,
  });

  String nombreUsuario;
  String apellidoUsuario;
  String emailUsuario;
  String passwordUsuario;
  bool estado;
  String roles;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        nombreUsuario: json["nombreUsuario"],
        apellidoUsuario: json["apellidoUsuario"],
        emailUsuario: json["emailUsuario"],
        passwordUsuario: json["passwordUsuario"],
        estado: json["estado"],
        roles: json["roles"],
      );

  Map<String, dynamic> toJson() => {
        "nombreUsuario": nombreUsuario,
        "apellidoUsuario": apellidoUsuario,
        "emailUsuario": emailUsuario,
        "passwordUsuario": passwordUsuario,
        "estado": estado,
        "roles": roles,
      };
}
