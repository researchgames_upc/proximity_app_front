// To parse this JSON data, do
//
//     final marcador = marcadorFromJson(jsonString);

import 'dart:convert';

Marcador marcadorFromJson(String str) => Marcador.fromJson(json.decode(str));

String marcadorToJson(Marcador data) => json.encode(data.toJson());

class Marcador {
  static const double size = 25;
    Marcador({
        this.nombreLugar,
        this.estadoLugar,
        this.latitud,
        this.longitud,
        this.descripcion,
        this.imagen,
    });

    String nombreLugar;
    int estadoLugar;
    num latitud;
    num longitud;
    String descripcion;
    String imagen;

    factory Marcador.fromJson(Map<String, dynamic> json) => Marcador(
        nombreLugar: json["nombreLugar"],
        estadoLugar: json["estadoLugar"],
        latitud: json["latitud"].toDouble(),
        longitud: json["longitud"].toDouble(),
        descripcion: json["descripcion"],
        imagen: json["imagen"],
    );

    Map<String, dynamic> toJson() => {
        "nombreLugar": nombreLugar,
        "estadoLugar": estadoLugar,
        "latitud": latitud,
        "longitud": longitud,
        "descripcion": descripcion,
        "imagen": imagen,
    };
}
