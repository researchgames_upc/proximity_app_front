// To parse this JSON data, do
//
//     final place = placeFromJson(jsonString);

import 'dart:convert';

Place placeFromJson(String str) => Place.fromJson(json.decode(str));

String placeToJson(Place data) => json.encode(data.toJson());

class Place {
  Place({
    this.idLugar,
    this.nombreLugar,
    this.estadoLugar,
    this.latitud,
    this.longitud,
  });
  int idLugar;
  String nombreLugar;
  bool estadoLugar;
  num latitud;
  num longitud;

  factory Place.fromJson(Map<String, dynamic> json) => Place(
        idLugar: json["idLugar"],
        nombreLugar: json["nombreLugar"],
        estadoLugar: json["estadoLugar"],
        latitud: json["latitud"],
        longitud: json["longitud"],
      );

  Map<String, dynamic> toJson() => {
        "idLugar": idLugar,
        "nombreLugar": nombreLugar,
        "estadoLugar": estadoLugar,
        "latitud": latitud,
        "longitud": longitud,
      };
}
